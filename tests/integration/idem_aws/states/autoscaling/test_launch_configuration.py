import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_launch_configuration(hub, ctx, aws_image):
    launch_configuration_temp_name = "idem-test-launch_configuration-" + str(
        uuid.uuid4()
    )
    image_id = aws_image

    # Create launch configuration with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await hub.states.aws.autoscaling.launch_configuration.present(
        test_ctx,
        name=launch_configuration_temp_name,
        instance_type="t2.micro",
        image_id=image_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.autoscaling.launch_configuration '{launch_configuration_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    created_configuration_name = resource.get("name")
    resource_id = resource.get("resource_id")

    assert "t2.micro" == ret["new_state"]["instance_type"]
    assert image_id == ret["new_state"]["image_id"]
    assert ret["result"], ret["comment"]
    assert resource_id == launch_configuration_temp_name
    assert created_configuration_name == launch_configuration_temp_name

    # Create launch configuration in real
    ret = await hub.states.aws.autoscaling.launch_configuration.present(
        ctx,
        name=launch_configuration_temp_name,
        instance_type="t2.micro",
        image_id=image_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"Created aws.autoscaling.launch_configuration '{launch_configuration_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    created_configuration_name = resource.get("name")
    resource_id = resource.get("resource_id")

    assert "t2.micro" == ret["new_state"]["instance_type"]
    assert image_id == ret["new_state"]["image_id"]
    assert ret["result"], ret["comment"]

    # verify that created launch_configuration is present (describe)
    describe_ret = await hub.states.aws.autoscaling.launch_configuration.describe(ctx)
    assert created_configuration_name in describe_ret
    assert "aws.autoscaling.launch_configuration.present" in describe_ret.get(
        resource_id
    )

    described_resource = describe_ret.get(resource_id).get(
        "aws.autoscaling.launch_configuration.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "t2.micro" == described_resource_map.get("instance_type")
    assert image_id == described_resource_map.get("image_id")
    assert launch_configuration_temp_name == described_resource_map.get("name")

    # Create launch configuration again with same resource_id (no change in state)
    ret = await hub.states.aws.autoscaling.launch_configuration.present(
        ctx,
        name=launch_configuration_temp_name,
        resource_id=resource_id,
        instance_type="t2.micro",
        image_id=image_id,
    )
    assert (
        f"aws.autoscaling.launch_configuration '{launch_configuration_temp_name}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Launch Configurations cannot be updated after creation with the Amazon Web Service API.
    # present state with same resource_id and with different instance_type will not update launch configuration resource
    test_ctx["test"] = True
    ret = await hub.states.aws.autoscaling.launch_configuration.present(
        test_ctx,
        name=launch_configuration_temp_name,
        resource_id=resource_id,
        instance_type="t2.small",
        image_id=image_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.autoscaling.launch_configuration '{launch_configuration_temp_name}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")

    assert ret["old_state"]["instance_type"] == ret["new_state"]["instance_type"]
    assert ret["old_state"]["image_id"] == ret["new_state"]["image_id"]
    assert ret["result"], ret["comment"]
    assert resource_id == launch_configuration_temp_name

    # With real context
    ret = await hub.states.aws.autoscaling.launch_configuration.present(
        ctx,
        name=launch_configuration_temp_name,
        resource_id=resource_id,
        instance_type="t2.small",
        image_id=image_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"aws.autoscaling.launch_configuration '{launch_configuration_temp_name}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")

    assert ret["old_state"]["instance_type"] == ret["new_state"]["instance_type"]
    assert ret["old_state"]["image_id"] == ret["new_state"]["image_id"]
    assert ret["result"], ret["comment"]

    # Delete launch configuration with test flag
    ret = await hub.states.aws.autoscaling.launch_configuration.absent(
        test_ctx, name=launch_configuration_temp_name, resource_id=resource_id
    )
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.autoscaling.launch_configuration",
            name=launch_configuration_temp_name,
        )[0]
        in ret["comment"]
    )
    # Delete launch configuration in real
    ret = await hub.states.aws.autoscaling.launch_configuration.absent(
        ctx, name=launch_configuration_temp_name, resource_id=resource_id
    )
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.autoscaling.launch_configuration",
            name=launch_configuration_temp_name,
        )[0]
        in ret["comment"]
    )

    # Deleting the same launch configuration again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.autoscaling.launch_configuration.absent(
        ctx, name=launch_configuration_temp_name, resource_id=resource_id
    )
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.autoscaling.launch_configuration",
            name=launch_configuration_temp_name,
        )[0]
        in ret["comment"]
    )

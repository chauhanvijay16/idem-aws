import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-placement-group-" + str(int(time.time())),
    "strategy": "partition",
    "partition_count": 3,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):

    global PARAMETER
    ctx["test"] = __test
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}

    ret = await hub.states.aws.ec2.placement_group.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]

    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.ec2.placement_group '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["group_name"]
        assert (
            f"Created aws.ec2.placement_group '{PARAMETER['name']}'" in ret["comment"]
        )

    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("group_name")
    assert PARAMETER["strategy"] == resource.get("strategy")
    assert PARAMETER["partition_count"] == resource.get("partition_count")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.ec2.placement_group.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.ec2.placement_group.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.ec2.placement_group.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["name"] == described_resource_map.get("group_name")
    assert PARAMETER["strategy"] == described_resource_map.get("strategy")
    assert PARAMETER["partition_count"] == described_resource_map.get("partition_count")
    assert PARAMETER["tags"] == described_resource_map.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.placement_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["name"] == old_resource.get("group_name")
    assert PARAMETER["strategy"] == old_resource.get("strategy")
    assert PARAMETER["partition_count"] == old_resource.get("partition_count")
    if __test:
        assert (
            f"Would delete aws.ec2.placement_group '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Deleted aws.ec2.placement_group '{PARAMETER['name']}'" in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.placement_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )

    assert ret["result"]
    assert ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.ec2.placement_group '{PARAMETER['name']}' already absent"
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_placement_group_absent_with_none_resource_id(hub, ctx):
    name = PARAMETER["name"]

    # Delete placement group with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ec2.placement_group.absent(
        ctx, name=name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.placement_group '{name}' already absent" in ret["comment"]


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ec2.placement_group.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
